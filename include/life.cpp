/**
* Implement life class functions
*/
#include "life.h"

/*
 * Class's constructor
*/
explicit Life( T row, T col );

/*
 * Class's destructor
*/
~Life();

/*
 * Update the board and create new generations
*/
void update( const T& x );

/*
 * Display the board
*/
void print( const T& x );

/*
 * Set a specific cell as alive
 * @ x => x coordinate
 * @ y => y coordinate
*/
bool setAlive( T x_, T y_ );

/*
 * Check if a specific cell is dead
*/
bool isDead( T x_, T y_ );

/*
 * Check if generation is extinct
*/
bool extinct( );

/*
 * Check if generation is stable
*/
bool stable( );