#include <iostream>
#include "life.h"

using namespace std;

/*
 * =======LIFE FLOW=======
 * 1 - Read input data
 * 2 - Generate data struct (matrix)
 * 3 - Print initial arrangement
 * 4 - User input (define if new generations'll be created)
 * 5 - Define a class to handle rules ( class life )

 * =========RULES=========
 * 1 - Handle wrong inputs and display a custom message (invalid arguments)
*/

/* =====Constants===== */
 #define DEAD  0
 #define ALIVE 1
/* =================== */

int main ( int argc, char *argv[] )
{
	// [1] Validate input arguments
	cout << "Argc value: " << argc << endl;

	for ( int i(0); i < argc; ++i )
		cout << "argc[" << i << "]: " << argv[i] << endl;

	if ( argc < 2 )
	{
		cerr << "erro....";
		
	}

	// ****************************************************

	// [2] Read input file
	/* These values are dummy, need to get from config file */
	int iRows = 10;
	int iCols = 10;
	bool user_input = true;
	int board[iRows][iCols] = {
		{0, 0, 0, 0, 0, 0, 1, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
		{0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
		{0, 0, 0, 1, 1, 0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 1, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
		{0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
	}

	// [3] Initial setting according to input data
	Life iLife( iRows, iCols );
	Life previousiLife( iRows, iCols );

	// [4] Initialize the board with alive cells
	for ( int i(0); i < iRows; ++i )
	{
		// Cols
		for ( int j(0); j < iCols; ++j )
		{
			// Check if the cell is alive
			if ( board[i][j] == ALIVE )
			{
				// Set the object cell as alive
				iLife.setAlive(i, j);
			}
		}
	}

	// [5] Print first generation
	iLife.print();

	// [6] Print next generations if asked
	while ( user_input == true && !iLife.stable() && !iLife.extinct() )
	{
		// Apply simulation rules
		iLife.update();

		// Print in terminal
		iLife.print();

		// Ask user about print the new generations
		cout << "Would you like to see the new generation? (y, n)" << endl;
		cin >> user_input;
	}

    return 0;
}